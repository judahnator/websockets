<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Middleware;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\DeferredBroadcast;
use Peachtree\Websocket\IO\Interfaces\Response;
use Peachtree\Websocket\Message;

final class Debugger implements Middleware
{
    private int $maxWidth;

    public function __construct(int $cols = 60)
    {
        $this->maxWidth = $cols - 3;
    }

    public function __invoke(Generator $responses, Message $input, State &$state): Generator
    {
        echo $this->excerpt($state->getIdentifier() ?: 'Unknown Connection ID'), PHP_EOL;

        echo $this->excerpt(sprintf(' < %s', (string)$input)), PHP_EOL;

        foreach ($responses as $response) {
            /** @var Response $response */
            echo $this->excerpt(
                sprintf(
                    ' > %s',
                    $response instanceof DeferredBroadcast
                        ? 'deferred broadcast'
                        : (string)$response->getMessage()
                )
            ), PHP_EOL;
            yield $response;
        }

        echo str_repeat('-', $this->maxWidth + 3), PHP_EOL;
    }

    private function excerpt(string $input): string
    {
        if (strlen($input) > $this->maxWidth) {
            return substr($input, 0, $this->maxWidth) . '...';
        }
        return $input;
    }
}
