<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Support;

use Generator;
use Iterator;

/**
 * Class GeneratorIterator
 * @package Peachtree\Websocket\Support
 * @template T
 * @implements Iterator<T>
 */
class GeneratorIterator implements Iterator
{
    /** @var Generator<T> */
    private Generator $generator;

    /**
     * GeneratorIterator constructor.
     * @param Generator<T> $generator
     */
    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param iterable<T> $iterator
     * @return self<Generator<T>>
     */
    public static function make(iterable $iterator): self
    {
        return new self((static function () use (&$iterator): Generator {
            yield from $iterator;
        })());
    }

    /**
     * @return T
     */
    public function current()
    {
        return $this->generator->current();
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        $this->generator->next();
    }

    /**
     * @inheritDoc
     * @return string|float|int|bool|null
     */
    public function key()
    {
        return $this->generator->key();
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return $this->generator->valid();
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        $this->generator->rewind();
    }
}
