<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Connection;

use DateTime;
use stdClass;

final class State
{

    /** @var string[] */
    protected array $channels = [];

    /** @var ?string */
    protected ?string $identifier;

    /** @var ?DateTime */
    protected ?DateTime $lastSeen = null;

    /** @var stdClass */
    protected stdClass $meta;

    public function __construct(string $identifier = null, stdClass $metadata = null)
    {
        $this->identifier = $identifier;
        $this->meta = $metadata ?: new stdClass();
    }

    /**
     * Prints debug information about this connection state.
     *
     * @return array<string, mixed>
     */
    public function __debugInfo(): array
    {
        return [
            'channels' => $this->channels,
            'identifier' => $this->identifier,
            'lastSeen' => $this->lastSeen ? $this->lastSeen->format('r') : null,
            'meta' => $this->meta,
        ];
    }

    /**
     * Return the identifier this connection state.
     *
     * @return ?string
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * Returns the metadata object associated with this connection state.
     *
     * @return stdClass
     */
    public function &getMeta(): stdClass
    {
        return $this->meta;
    }

    /**
     * Note that we have seen this connection.
     */
    public function see(): void
    {
        $this->lastSeen = new DateTime();
    }

    /**
     * Subscribe to a new channel.
     *
     * @param string $channel
     */
    public function subscribeToChannel(string $channel): void
    {
        if ($this->subscribedToChannel($channel)) {
            return;
        }
        $this->channels[] = $channel;
    }

    /**
     * Checks if this state has a given subscription.
     *
     * @param string $channel
     * @return bool
     */
    public function subscribedToChannel(string $channel): bool
    {
        return in_array($channel, $this->channels);
    }

    /**
     * Unsubscribe from a given channel.
     *
     * @param string $channel
     */
    public function unsubscribeFromChannel(string $channel): void
    {
        if (($key = array_search($channel, $this->channels, true)) !== false) {
            unset($this->channels[$key]);
        }
    }
}
