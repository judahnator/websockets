<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Handler;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\Collection;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\IO\Interfaces\Response as ResponseInterface;

/**
 * If code needs to interact with websocket messages, that must be done through a class that extends this.
 * Register new message handlers within the websocket router.
 *
 * Note that multiple handlers can be applied to each message.
 *
 * @package App\Websocket\Handlers
 */
abstract class MessageHandler
{

    /**
     * Responsible for handling the message.
     *
     * @param Message $message
     * @param State $state
     * @return Collection
     */
    final public function __invoke(Message $message, State &$state): Collection
    {
        return new Collection($this->handle($message, $state));
    }

    /**
     * Do whatever needs done with this message.
     * If a Response is yielded, that message will be sent back to the client.
     * If a Broadcast is yielded, it will be sent to subscribers on those channels.
     *
     * @param Message $message
     * @param State $state
     * @return Generator<ResponseInterface>
     */
    abstract protected function handle(Message $message, State &$state): Generator;

    /**
     * Indicator for if this message handler should be responsible for this message.
     * Should be non-blocking and as fast as possible.
     *
     * @param string $messageAction
     * @return bool
     */
    abstract public function shouldHandle(string $messageAction): bool;
}
