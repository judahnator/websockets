<?php

declare(strict_types=1);

namespace Peachtree\Websocket\IO;

use Peachtree\Websocket\IO\Interfaces\Response as ResponseInterface;
use Peachtree\Websocket\Message;

final class Response implements ResponseInterface
{
    private Message $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Return the message associated with this class.
     *
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }
}
