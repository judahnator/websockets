<?php

declare(strict_types=1);

namespace Peachtree\Websocket\IO;

use Generator;
use Peachtree\Websocket\IO\Interfaces\Response;
use Peachtree\Websocket\Support\GeneratorIterator;

/**
 * Class Collection
 * @package Peachtree\Websocket\IO
 * @extends GeneratorIterator<Response>
 */
final class Collection extends GeneratorIterator
{
    /**
     * Collection constructor.
     * @param Generator<Response> $responses
     */
    public function __construct(Generator $responses)
    {
        parent::__construct($responses);
    }

    public function current(): Response
    {
        return parent::current();
    }
}
