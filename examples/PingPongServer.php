<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Peachtree\Websocket as WS;

// Setup the server with its default config
$server = new WS\Server();

// Add the built-in "ping-pong" message handler to the router
WS\Routing\Router::addMessageHandler(
    new WS\Handler\PingPong()
);

// Start the server
$server->socket()->run();

/*
 * Now that the server is running, you can connect to it and send it messages.
 * Remember the WS\Connection\Handler requires that all messages be JSON-encoded, with a reference, payload, and action.
 *
 * For example, if you telnet into the server you can send:
 *  > {"action":"ping","payload":{},"ref":"ping"}
 * And the server will respond with:
 *  < {"ref":"ping","action":"pong","payload":{}}
 */
