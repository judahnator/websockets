<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Peachtree\Websocket as WS;
use Respect\Validation\Exceptions\ValidationException;
use Respect\Validation\Rules as Rule;
use Respect\Validation\Validator;

// Setup the server with its default config
$server = new WS\Server();

// Create custom message handler for placing bets
$gambleHandler = new class extends WS\Handler\MessageHandler {
    /**
     * @inheritDoc
     */
    protected function handle(WS\Message $message, WS\Connection\State &$state): Generator
    {
        // We need to make sure the clients state is initialized
        if (!property_exists($state->getMeta(), 'balance')) {
            // If it is not initialized, set their balance to 100 because why not
            $state->getMeta()->balance = 100;
        }

        // Get the balance
        $balance =& $state->getMeta()->balance;

        // Lets validate our input, now that we know our max bet
        try {
            Validator::create()
                ->addRule(new Rule\Key('bet', new Rule\AllOf(
                    new Rule\Min(0, false), // zero-bets are off
                    new Rule\Max($balance, true), // cannot bet more than your balance
                    new Rule\Max(100, true), // Cannot bet more than 100
                )))
                ->check($message->getPayload());
        } catch (ValidationException $e) {
            // Validation failed, tell the client why and abort
            yield new WS\IO\Response(
                WS\MessageFactory::make($message->getRef())->validationException($e->getMainMessage())
            );
            return;
        }

        // Grab the bet so we dont have access the payload everywhere
        $bet = $message->getPayload()['bet'];

        // Lets make our odds 1/(201-{bet}), the house must always win of course
        $win = rand(1, 201 - $bet) === 1;

        // If we won or lost, tell the client and adjust their balance
        if ($win) {
            yield new WS\IO\Response(
                WS\MessageFactory::make($message->getRef())->acknowledge('You win ' . (string)$bet)
            );
            $balance += $bet;
        } else {
            yield new WS\IO\Response(
                WS\MessageFactory::make($message->getRef())->acknowledge('You lose ' . (string)$bet)
            );
            $balance -= $bet;
        }
    }

    /**
     * @inheritDoc
     */
    public function shouldHandle(string $messageAction): bool
    {
        return $messageAction === 'play';
    }
};

// Our users might want to check their balance, lets create a handler to check that for them
$balanceHandler = new class extends WS\Handler\MessageHandler {
    /**
     * @inheritDoc
     */
    protected function handle(WS\Message $message, WS\Connection\State &$state): Generator
    {
        // If the client has no balance, set it to a default of 100
        if (!property_exists($state->getMeta(), 'balance')) {
            $state->getMeta()->balance = 100;
        }
        yield new WS\IO\Response(
            WS\MessageFactory::make($message->getRef())->acknowledge('Your balance is ' . (string)($state->getMeta()->balance))
        );
    }

    /**
     * @inheritDoc
     */
    public function shouldHandle(string $messageAction): bool
    {
        return $messageAction === 'balance';
    }
};

// Add our custom handler to the router
WS\Routing\Router::addMessageHandler($gambleHandler, $balanceHandler);

// Start the server
$server->socket()->run();

/*
 * Connecting to our server, we can check our balance easy enough by this command:
 *  > {"action":"balance","payload":{},"ref":"get balance"}
 * Since we have not played yet, we can see that we have a balance of 100:
 *  < {"ref":"get balance","action":"ack","payload":{"message":"Your balance is 100"}}
 *
 * Lets play a bet, say 20:
 *  > {"action":"play","payload":{"bet":20},"ref":"bet-20"}
 *  < {"ref":"bet-20","action":"ack","payload":{"message":"You lose 20"}}
 *
 * Looks like we lost. We better re-check our balance
 *  > {"action":"balance","payload":{},"ref":"get balance again"}
 *  < {"ref":"get balance again","action":"ack","payload":{"message":"Your balance is 80"}}
 */
