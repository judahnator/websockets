Unreleased Changes:
==================

* Added PHP 8.0 support.

v2.0.0:
=======

Backwards Incompatibility:
--------------------------
* The `\Peachtree\Websocket\Routing\RouterInterface` signature has changed. If you are using the built-in message router then this change will not affect you.
  * The `handle` method has been renamed to `handleMessage`.
  * A new `handleError` method has been added.
  
Changes:
--------
* Error logging functionality has been added, to help troubleshoot any errors that might arise from the message handlers. Previously if a message handler threw an exception, the connection would be closed, and the exception ignored. With this update you can add a callback to record/report runtime errors.
* Adding debug info to the `\Peachtree\Websocket\Connection\State` class.

Usage:
```php
use Exception;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\Routing\Router;

Router::addErrorHandler(
    function (Exception $e, State $state): void {
        // However you want to log/report the exception.
        // Also passing the connection state to help troubleshoot.    
    }
);
```

v1.2.2
======

Backwards Incompatibility:
--------------------------
None!

Fixes:
------
* Corrects a regression where an incoming message's payload could be incorrectly cast. 

v1.2.1
======

Backwards Incompatibility:
--------------------------
None!

Fixes:
------
* Fixing an issue where a variables' usage was before its definition.

v1.2.0
======

Backwards Incompatibility:
--------------------------
None!

Changes:
--------
* Minor optimizations in the `\Peachtree\Websocket\Server` class to reduce complexity.
* Clients may now send multiple messages packed into a single array.

v1.1.0
======

Backwards Incompatibility:
--------------------------
None!

Changes:
--------
Allows the usage of application middleware.

Upgrade Notes:
-------------- 
If you are using your own custom `\Peachtree\Websocket\Routing\RouterInterface` implementation, be aware that the new middleware functionality will not be available by default. Reference the default implementation (`\Peachtree\Websocket\Routing\Router`) for an implementation guide.

V1.0.0
======

Initial public release.
