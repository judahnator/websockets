<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Connection;

use Peachtree\Websocket\Connection\State;
use PHPUnit\Framework\TestCase;

final class StateTest extends TestCase
{
    public function testStateClass(): void
    {
        $state = new State();
        $this->assertNull($state->getIdentifier());
        $this->assertEquals(new \stdClass(), $state->getMeta());

        $meta = (object)['foo' => 'bar'];
        $state = new State('asdf', $meta);
        $this->assertEquals('asdf', $state->getIdentifier());
        $this->assertEquals($meta, $state->getMeta());

        // Test updating meta to ensure we are looking at a reference
        $meta->foo = 'bing';
        $state->getMeta()->foo = 'bing';
        $this->assertEquals($meta, $state->getMeta());
    }

    public function testChannelSubscriptions(): void
    {
        $state = new State();

        $this->assertFalse($state->subscribedToChannel('foo'));
        $state->subscribeToChannel('foo');
        $this->assertTrue($state->subscribedToChannel('foo'));
        $state->unsubscribeFromChannel('foo');
        $this->assertFalse($state->subscribedToChannel('foo'));
    }

    public function testDebugInfo(): void
    {
        $state = new State('foo bar', (object)['bing' => 'baz']);
        $state->subscribeToChannel('foo channel');
        $state->see();
        $this->assertEquals(
            sprintf(
                <<<TEXT
                Peachtree\Websocket\Connection\State Object
                (
                    [channels] => Array
                        (
                            [0] => foo channel
                        )
                
                    [identifier] => foo bar
                    [lastSeen] => %s
                    [meta] => stdClass Object
                        (
                            [bing] => baz
                        )
                
                )
                
                TEXT,
                date('r'),
            ),
            print_r($state, true),
        );
    }
}
