<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Handlers;

use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\Handler\ChannelManager;
use Peachtree\Websocket\Message;
use PHPUnit\Framework\TestCase;

final class ChannelManagerTest extends TestCase
{
    public function testFailedValidation(): void
    {
        $handler = new ChannelManager();
        $state = new State();

        $message = (new Message())
            ->setAction('channel')
            ->setPayload([])
            ->setRef('foo bar');
        $response = $handler($message, $state)->current()->getMessage();
        $this->assertEquals('error', $response->getAction());
        $this->assertEquals(
            [
                'code' => 422,
                'reason' => [
                    'Key subscribe must be present',
                    'Key unsubscribe must be present'
                ],
                'message' => 'All of the required rules must pass for channel management validation'
            ],
            $response->getPayload()
        );
        $this->assertEquals('foo bar', $response->getRef());

        $message = (new Message())
            ->setAction('channel')
            ->setPayload([
                'subscribe' => ['foo'],
            ])
            ->setRef('bing baz');
        $response = $handler($message, $state)->current()->getMessage();
        $this->assertEquals('error', $response->getAction());
        $this->assertEquals(
            [
                'code' => 422,
                'reason' => [
                    'Key unsubscribe must be present'
                ],
                'message' => 'These rules must pass for channel management validation'
            ],
            $response->getPayload()
        );
        $this->assertEquals('bing baz', $response->getRef());
    }

    public function testJoiningChannel(): void
    {
        $handler = new ChannelManager();
        $state = new State();

        $message = (new Message())
            ->setAction('channel')
            ->setPayload([
                'subscribe' => ['foo'],
                'unsubscribe' => []
            ])
            ->setRef('foo bar');
        $response = $handler($message, $state)->current()->getMessage();
        $this->assertEquals('ack', $response->getAction());
        $this->assertEquals(
            ['message' => 'Subscribed to 1 and unsubscribed from 0 channels.'],
            $response->getPayload()
        );
        $this->assertEquals('foo bar', $response->getRef());
        $this->assertTrue($state->subscribedToChannel('foo'));
        $this->assertFalse($state->subscribedToChannel('bar'));

        $message = (new Message())
            ->setAction('channel')
            ->setPayload([
                'subscribe' => ['bar', 'baz'],
                'unsubscribe' => ['foo']
            ])
            ->setRef('bing baz');
        $response = $handler($message, $state)->current()->getMessage();
        $this->assertEquals('ack', $response->getAction());
        $this->assertEquals(
            ['message' => 'Subscribed to 2 and unsubscribed from 1 channels.'],
            $response->getPayload()
        );
        $this->assertEquals('bing baz', $response->getRef());
        $this->assertFalse($state->subscribedToChannel('foo'));
        $this->assertTrue($state->subscribedToChannel('bar'));
        $this->assertTrue($state->subscribedToChannel('baz'));
    }

    public function testShouldHandle(): void
    {
        $handler = new ChannelManager();
        $this->assertTrue($handler->shouldHandle('channel'));
        $this->assertFalse($handler->shouldHandle('not channel'));
    }

    public function testValidation(): void
    {
        $handler = new ChannelManager(
            fn (string $channel): bool => in_array($channel, ['allowed', 'also allowed'])
        );
        $state = new State();

        $expected = [
            (new Message())
                ->setAction('error')
                ->setPayload(['code' => 403, 'message' => 'You are not authorized to join "foo channel"'])
                ->setRef('foo bar'),
            (new Message())
                ->setAction('ack')
                ->setPayload(['message' => 'Subscribed to 2 and unsubscribed from 0 channels.'])
                ->setRef('foo bar')
        ];

        $responses = $handler(
            (new Message())
                ->setAction('channel')
                ->setPayload([
                    'subscribe' => ['allowed', 'also allowed', 'foo channel'],
                    'unsubscribe' => [],
                ])
                ->setRef('foo bar'),
            $state
        );

        foreach ($responses as $num => $response) {
            $this->assertEquals($expected[$num], $response->getMessage());
        }
    }
}
