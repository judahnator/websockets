<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Middleware;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\Response;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\MessageFactory;
use Peachtree\Websocket\Middleware\Debugger;
use PHPUnit\Framework\TestCase;

final class DebuggerMiddlewareTest extends TestCase
{
    public function testDebuggerOutput(): void
    {
        $middleware = new Debugger();

        /** @var Generator $response */
        $response = (fn (): Generator => yield new Response(MessageFactory::make()->acknowledge('fdsa')))();
        $message = (new Message())
            ->setAction('foo')
            ->setPayload(['bing' => 'baz'])
            ->setRef(null);
        $state = new State('Some ID');

        ob_start();

        // This assertion just starts the generator
        $this->assertEquals(
            $response->current(),
            $middleware(
                $response,
                $message,
                $state
            )->current()
        );
        $output = ob_get_contents();

        ob_end_clean();

        $this->assertEquals(
            <<<TEXT
Some ID
 < {"ref":null,"action":"foo","payload":{"bing":"baz"}}
 > {"ref":null,"action":"ack","payload":{"message":"fdsa"...

TEXT,
            $output
        );
    }
}
