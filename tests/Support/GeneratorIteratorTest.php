<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Support;

use ArrayIterator;
use Peachtree\Websocket\Support\GeneratorIterator;
use PHPUnit\Framework\TestCase;

final class GeneratorIteratorTest extends TestCase
{
    public function testMakingGeneratorIterators(): void
    {
        $expectedArray = ['foo', 'bar'];

        $GI = GeneratorIterator::make($expectedArray);
        foreach ($expectedArray as $key => $expected) {
            $this->assertEquals($key, $GI->key());
            $this->assertEquals($expected, $GI->current());
            $GI->next();
        }

        $GI = GeneratorIterator::make(new ArrayIterator($expectedArray));
        foreach ($expectedArray as $key => $expected) {
            $this->assertEquals($key, $GI->key());
            $this->assertEquals($expected, $GI->current());
            $GI->next();
        }
    }

    public function testGeneratorRewind(): void
    {
        $GI = GeneratorIterator::make(range(1, 10));
        $GI->rewind(); // should not throw exception, not open
        $this->assertTrue(true); // Just to keep test suit happy
    }

    public function testGeneratorRewindException(): void
    {
        $this->expectExceptionMessage('Cannot rewind a generator that was already run');
        $GI = GeneratorIterator::make([0]);
        while ($GI->valid()) {
            $GI->next();
        }
        $GI->rewind();
    }
}
