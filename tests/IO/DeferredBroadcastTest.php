<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\IO;

use Exception;
use Peachtree\Websocket\IO\DeferredBroadcast;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\MessageFactory;
use PHPUnit\Framework\TestCase;

final class DeferredBroadcastTest extends TestCase
{
    public function testCreatingDeferredBroadcast(): void
    {
        $messageResolver = static function (Message &$message): callable {
            return function () use (&$message): Message {
                return $message;
            };
        };

        $message = (new Message())
            ->setAction('test')
            ->setPayload(['foo' => 'bar'])
            ->setRef('asdf');

        $channels = ['foo', 'bar', 'baz'];

        $broadcast = new DeferredBroadcast($messageResolver($message), ...$channels);

        $this->assertEquals($message, $broadcast->getMessage());
        $this->assertEquals($channels, $broadcast->getChannels());
    }

    public function testMessageResolverGetsRanOnOpen(): void
    {
        $messageOpened = false;
        $broadcast = new DeferredBroadcast(function () use (&$messageOpened): Message {
            $messageOpened = true;
            return MessageFactory::make()->acknowledge('asdf');
        });
        $broadcast->getMessage();
        $this->assertTrue($messageOpened);
    }

    public function testMessageResolverNotOpenedPrematurely(): void
    {
        $broadcast = new DeferredBroadcast(static function (): void {
            throw new Exception('Broadcast opened resolver prematurely.');
        });
        $this->assertEmpty($broadcast->getChannels()); // just used to interact with the broadcast and ensure no exception thrown
    }
}
