<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\IO;

use Peachtree\Websocket\IO\Response;
use Peachtree\Websocket\Message;
use PHPUnit\Framework\TestCase;

final class ResponseTest extends TestCase
{
    public function testResponse(): void
    {
        $message = (new Message())
            ->setAction('foo')
            ->setPayload(['bar'])
            ->setRef('bing');

        $broadcast = new Response($message);

        $this->assertEquals($message, $broadcast->getMessage());
    }
}
